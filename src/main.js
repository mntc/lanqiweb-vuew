import Vue from 'vue'
import App from './App.vue'
import router from './router/';
import axios from 'axios';
Vue.config.productionTip = false

Vue.prototype.$http = axios
axios.defaults.baseURL = 'http://localhost:29492'
axios.defaults.timeout = 30000;
// 请求拦截器
 axios.interceptors.request.use(config => {
  const token = localStorage.getItem('jwtToken'+"18.38");
  console.log(token+" 全局守卫");
  if (token) {
    config.headers.Authorization = `Bearer ${token}`;
  }
  return config;
}, error => {
  return Promise.reject(error);
}); 


new Vue({
	router,
  render: h => h(App),
}).$mount('#app')
