import Vue from 'vue';
import Router from 'vue-router';
import Dashboard from '@/components/Dashboard.vue';
import Login from '@/components/HelloWorld.vue';

Vue.use(Router);

const router = new Router({
  routes: [
	 {  path: '/', // 根路径重定向到'/login'
	      redirect: '/login'
	    },
    {
      path: '/login',
      name: 'Login',
      component: Login
    },
    {
      path: '/dashboard',
      name: 'Dashboard',
      component: Dashboard,
      meta: { requiresAuth: true } // 添加meta信息，表示需要认证
    }
  ]
});

// 全局前置守卫
 router.beforeEach((to, from, next) => {
  // 如果目标路由需要认证并且用户未登录
   if (to.meta.requiresAuth && !localStorage.getItem('jwtToken')) {
     // 重定向到登录页
     next('/login');
   } else {
     // 用户已登录或者目标路由不需要认证，直接进入目标页面
     next();
   }
}); 

export default router;